var cardsImg = [];
var cardsObjs = [];
var quantity = localStorage.getItem('quantity');
var oneVisible = false;
var turnCounter = 0;
var lock = false;
var pairsLeft = quantity/2;
var levelImg = localStorage.getItem('img');

//"geralt.png", "ciri.png", "jaskier.png", "triss.png", "yen.png", "iorweth.png", "geralt.png", "ciri.png", "jaskier.png", "triss.png", "yen.png", "iorweth.png"

//funkcja do mieszania tablicy
function shuffle(a) {
    var j, x, i;
    for (i = a.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = a[i];
        a[i] = a[j];
        a[j] = x;
    }
    return a;
}

function createCards(quantity){
    $('.board').css('background-image', levelImg);
    for(let i = 1; i <= quantity/2; i++){
        cardsImg.push(i + '.jpg');
        cardsImg.push(i + '.jpg');
    }    
    shuffle(cardsImg);
    cardsImg.unshift('początek');
    for(let i = 1; i <= quantity; i++){
        var elementName = 'c' + i;
        $('.board').append('<div class="card" id="'+ elementName + '"</div>');
        cardsObjs.push(document.getElementById(elementName));
    }
    for (let i = 1; i <= quantity; i++) {
        cardsObjs[i-1].addEventListener("click", function(){ revealCard(i) });
    }
}
createCards(quantity);

function revealCard(nr){
    
    var opacityValue = $('#c'+nr).css('opacity');  
//  alert("odrywasz kartê nr: " + nr);  
//    i = Math.floor((Math.random() * 100) + 1);
//    console.log(i);
   var audio = new Audio('sounds/2.mp3');
    audio.play();
if (opacityValue != 0 && lock == false)
{
   var obraz = "url(img/" + cardsImg[nr] +")";
levelImg
   $('#c'+nr).css('background-image', obraz);
   $('#c'+nr).addClass('cardA');
   $('#c'+nr).removeClass('card');

   if(oneVisible == false)
   {    
    //first card
    oneVisible = true
    visible_nr = nr;
    
} else {
    lock = true; 
//         second card         
if(cardsImg[visible_nr] == cardsImg[nr] && nr !=visible_nr){
//        alert('para');

setTimeout(function() { hide2cards(nr, visible_nr) }, 500);
turnCounter++;
pairsLeft--;
if(pairsLeft ==0 ){
    setTimeout(end(turnCounter), 750);
}

} else {
//        alert('pud³o');

turnCounter++;
setTimeout(function() { restore2Cards(nr, visible_nr) }, 1000);
}

$('.score').html('liczba rund: ' + turnCounter);
oneVisible = false;
     //alert(opacityValue);
 }
}
}

function hide2cards(nr1, nr2)
{
    $('#c'+nr1).css('opacity', '0');
    $('#c'+nr2).css('opacity', '0');

    lock = false;
}

function restore2Cards(nr1, nr2)
{
    $('#c'+nr1).css('background-image', 'url(img/karta.png)');
    $('#c'+nr1).addClass('card');
    $('#c'+nr1).removeClass('cardA');
    
    $('#c'+nr2).css('background-image', 'url(img/karta.png)');
    $('#c'+nr2).addClass('card');
    $('#c'+nr2).removeClass('cardA');

    lock = false;
}

function end(turnCounter){   
    $('#info').html('liczba rund: ' + turnCounter);
    localStorage.setItem('turnCounter', turnCounter);
    window.location.href = 'koniec.html';
}







